<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Tasks\TasksModel;

class CreateUpdateTaskJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $request;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $input = $this->request;
      $id = isset($input['id']) ? $input['id'] : '';
      $reference_id = (isset($input['reference_id']) && trim($input['reference_id']) != '') ? $input['reference_id'] : 0;
      $reference_uniqid = (isset($input['reference_uniqid']) && trim($input['reference_uniqid']) != '') ? $input['reference_uniqid'] : 0;
      TasksModel::create($input);
    }
}
