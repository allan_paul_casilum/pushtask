<?php

namespace App\Reminders;

use Illuminate\Database\Eloquent\Model;

class ReminderModel extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'reminders';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['task_id', 'title', 'description', 'send_every', 'send_every_custom', 'priority'];

  public function task()
  {
      return $this->belongsTo('App\Tasks\TasksModel');
  }
}
