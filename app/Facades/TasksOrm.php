<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class TasksOrm extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'TasksOrm';
    }
}
