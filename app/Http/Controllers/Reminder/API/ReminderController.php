<?php

namespace App\Http\Controllers\Reminder\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reminders\ReminderModel;

class ReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'task_id' => 'required',
          'title' => 'required',
      ]);
      $reminder = ReminderModel::create($request->all());

      return response()->json($reminder, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $reminder = ReminderModel::find($id);

      if (!$reminder) {
          return response()->json([
              'success' => false,
              'message' => 'Reminder with id ' . $id . ' not found'
          ], 400);
      }

      return response()->json([
          'success' => true,
          'data' => $reminder
      ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $reminder = ReminderModel::find($id);

      if (!$reminder) {
          return response()->json([
              'success' => false,
              'message' => 'Reminder with id ' . $id . ' not found'
          ], 400);
      }

      $updated = $reminder->update($request->all());

      if ($updated) {
          return response()->json([
              'success' => true,
              'data' => $reminder,
          ], 200);
      } else {
          return response()->json([
              'success' => false,
              'message' => 'Reminder could not be updated'
          ], 500);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
