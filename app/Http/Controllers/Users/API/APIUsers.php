<?php

namespace App\Http\Controllers\Users\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class APIUsers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user = User::create($request->all());

      //switch to resource later
      return response()->json($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $user = User::find($id);

      if (!$user) {
          return response()->json([
              'success' => false,
              'message' => 'User with id ' . $id . ' not found'
          ], 400);
      }
      $input = $request->all();
      $input['password'] = bcrypt($input['password']);

      $token = $user->createToken('PushTasksAllTeams')->accessToken;

      $updated = $user->update($input);

      if ($updated) {
          return response()->json([
              'success' => true,
              'data' => $user,
          ], 200);
      } else {
          return response()->json([
              'success' => false,
              'message' => 'User could not be updated'
          ], 500);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
