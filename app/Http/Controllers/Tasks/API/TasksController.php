<?php

namespace App\Http\Controllers\Tasks\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmailTask;
use App\Tasks\TasksModel;
use App\Tasks\TasksOrm;
use App\User;
use App\Jobs\SendEmailTaskJob;
use App\Jobs\CreateUpdateTaskJob;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = auth()->user();
      if($user->role == 'admin'){
        $data = TasksModel::paginate(10);
      }elseif($user->role == 'owner'){
        $data = TasksModel::where('parent', $user->id);
      }
      return response()->json(['user'=>$user->id,'data' => $data], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'user_id' => 'required',
          'title' => 'required',
          'description' => 'required',
          'assign_email' => 'required|email',
          'assign_name' => 'required',
          'status' => 'required',
      ]);

      $task_orm = new TasksOrm;

      //$task = TasksModel::create($request->all());
      $input = $request->all();
      $id = isset($input['id']) ? $input['id'] : '';
      $reference_id = (isset($input['reference_id']) && trim($input['reference_id']) != '') ? $input['reference_id'] : 0;
      $reference_uniqid = (isset($input['reference_uniqid']) && trim($input['reference_uniqid']) != '') ? $input['reference_uniqid'] : 0;

      $task = TasksModel::updateOrCreate(['reference_id' => $reference_id, 'reference_uniqid' => $reference_uniqid], $input);

      if ($task) {
          return response()->json([
              'success' => true,
              'data' => $task,
          ], 201);
      } else {
          return response()->json([
              'success' => false,
              'message' => 'Task could not be added'
          ], 500);
      }
      //return response()->json($task, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $task = TasksModel::find($id);

      if (!$task) {
          return response()->json([
              'success' => false,
              'message' => 'Tasks with id ' . $id . ' not found'
          ], 400);
      }

      return response()->json([
          'success' => true,
          'data' => $task
      ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = TasksModel::find($id);

        if (!$task) {
            return response()->json([
                'success' => false,
                'message' => 'Tasks with id ' . $id . ' not found'
            ], 400);
        }

        $updated = $task->update($request->all());

        if ($updated) {
            return response()->json([
                'success' => true,
                'data' => $task,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Task could not be updated'
            ], 500);
        }
    }

    public function getOwnerTasks()
    {
      $user = auth()->user();
      $user_id = $user->id;

      $task_orm = new TasksOrm;
      $task = $task_orm->getOwnerTasks($user_id);
      $data = [];
      foreach($task as $key => $val){
        $data[$val['assign_email']][] = $val;
        $data[$val['assign_email']]['reminders'] = $val->reminders;
      }
      return response()->json([
          'success' => true,
          'task' => $task,
          'data' => $data,
      ], 200);
    }

    public function sendTasksEmail()
    {
      $user = auth()->user();
      $user_id = $user->id;

      $task_orm = new TasksOrm;
      $task = $task_orm->getOwnerTasks($user_id);
      $task_data = [];

      foreach($task as $key => $val){
        $task_data[$val['assign_email']][] = $val;
      }

      foreach($task_data as $k => $v) {
        $mail_to = $k;
        $content_array = [];
        $content_array['count_task'] = count($v);

        foreach($v as $k_content => $v_content) {
          $content_array['content'][] = $v_content->description;
        }

        $data = [
          'email_to' => $mail_to,
          'content' => $content_array
        ];

        //no more waiting
        dispatch(new SendEmailTaskJob($data));
        //Mail::to($mail_to)->send(new SendEmailTask($data));
      }

    }

    public function getAllTasks()
    {
      $task_orm = new TasksOrm;
      $task = $task_orm->getAllTasks();
      $task_data = [];

      foreach($task as $key => $val){
        $task_data[$val['assign_email']][] = $val;
      }

      foreach($task_data as $k => $v) {
        $mail_to = $k;
        $content_array = [];
        $content_array['count_task'] = count($v);

        foreach($v as $k_content => $v_content) {
          $content_array['content'][] = $v_content->description;
        }

        $data[] = [
          'email_to' => $mail_to,
          'content' => $content_array
        ];
        //print_r($data);

        //no more waiting
        //dispatch(new SendEmailTaskJob($data));
      }
      return response()->json([
          'success' => true,
          'task' => $task_data,
          'data' => $data,
      ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $task = TasksModel::find($id);
      $task->delete();
    }

    public function updateTasks(Request $request)
    {
      $input = $request->all();
      $id = isset($input['id']) ? $input['id'] : '';
      $reference_id = (isset($input['reference_id']) && trim($input['reference_id']) != '') ? $input['reference_id'] : 0;
      $reference_uniqid = (isset($input['reference_uniqid']) && trim($input['reference_uniqid']) != '') ? $input['reference_uniqid'] : 0;
      $get = TasksModel::where('reference_id', $reference_id)
      ->where('reference_uniqid', $reference_uniqid)->first();
      if(!$get){
        return response()->json([
            'success' => false,
            'message' => 'No job to update',
        ], 400);
      }else{
        $task = TasksModel::find($get->id);
        $task->fill($input);
        $task->save();
        return response()->json([
            'success' => true,
            'task' => $task,
        ], 200);
      }
    }

    public function storeToJob(Request $request)
    {
      $input = $request->all();
      dispatch(new CreateUpdateTaskJob($input));
    }

    public function getReferenceId($user_id, $reference_id)
    {
      $get = TasksModel::where('reference_id', $reference_id)
                          ->where('user_id', $user_id)
                          ->get();
      if($get) {
        return response()->json([
            'success' => true,
            'task' => $task_data,
            'data' => $get,
        ], 200);
      }else{
        return response()->json([
            'success' => false,
            'data' => $get,
        ], 400);
      }

    }

    public function removeTasks(Request $request)
    {
      $input = $request->all();
      $this->validate($request, [
          'reference_id' => 'required',
          'reference_uniqid' => 'required',
      ]);
      
      $delete = TasksModel::where('reference_id', $input['reference_id'])
      ->where('reference_uniqid', $input['reference_uniqid'])->delete();

      if($delete){
        return response()->json([
            'success' => true,
            'message' => 'Deleted',
        ], 200);
      }else{
        return response()->json([
            'success' => false,
            'message' => 'Failed',
        ], 400);
      }

    }
}
