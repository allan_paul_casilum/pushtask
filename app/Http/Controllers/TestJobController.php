<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\TestSendWelcomeMail;
use Carbon\Carbon;

class TestJobController extends Controller
{
  public function processQueue()
  {
    $emailJob = (new TestSendWelcomeMail('Pankaj Sood'))->delay(Carbon::now()->addSeconds(3));
    dispatch($emailJob);
    echo 'Mail Sent';
  }
}
