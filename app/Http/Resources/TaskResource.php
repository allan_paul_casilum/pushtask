<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = auth()->user();
        if($user->role == 'admin'){
          $data = [
            'id' => $this->id,
            'user_id' => $this->user_id,
          ];
        }elseif($user->role == 'owner'){

        }
        return [
          'user' => $user,
          'data' => $data,
        ];
    }
}
