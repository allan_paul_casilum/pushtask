<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmailTask;
use App\Tasks\TasksModel;
use App\Tasks\TasksOrm;
use App\User;
use App\Jobs\SendEmailTaskJob;

class SendTaskReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:remind';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Reminder to Tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $task_orm = new TasksOrm;
      $task = $task_orm->getAllTasks();
      $task_data = [];

      foreach($task as $key => $val){
        $task_data[$val['assign_email']][] = $val;
      }

      foreach($task_data as $k => $v) {
        $mail_to = $k;
        $content_array = [];
        $content_array['count_task'] = count($v);

        foreach($v as $k_content => $v_content) {
          $content_array['content'][] = $v_content->description;
        }

        $data = [
          'email_to' => $mail_to,
          'content' => $content_array
        ];
        //print_r($data);
        //no more waiting
        dispatch(new SendEmailTaskJob($data));
      }
  }
}
