<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestWelcomeMail;
use Carbon\Carbon;

class TestSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test-schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      Mail::send('email-test',[], function($message) {
        $message->to('youremail@domain.com')->subject('Testing email');
      });
      $this->info('mail sent');
      /*$emailJob = (new TestSendWelcomeMail('Pankaj Sood'))->delay(Carbon::now()->addSeconds(3));
      dispatch($emailJob);*/
    }
}
