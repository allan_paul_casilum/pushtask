<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailTask extends Mailable
{
    use Queueable, SerializesModels;

    public $args;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($args)
    {
        /**
        * $args['email']
        * $args['content']
        **/
        $this->args = $args;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email-task')->with([
          'args' => $this->args
        ]);
    }
}
