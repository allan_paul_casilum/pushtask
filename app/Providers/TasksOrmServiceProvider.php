<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use App\Tasks\TasksOrm;

class TasksOrmServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('TasksOrm', function () {
            return new TasksOrm;
        });
    }
}
