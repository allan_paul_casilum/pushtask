<?php

namespace App\Tasks;
use App\Tasks\TasksModel;

class TasksOrm
{
    public function getOwnerTasks($user_id)
    {
      $task = TasksModel::where('user_id', $user_id)
                ->where('status', 'start')
                ->orderBy('assign_email')
                ->get();
      return $task;
    }

    public function getAllTasks()
    {
      $task = TasksModel::where('status', 'start')
                ->orderBy('assign_email')
                ->get();
      return $task;
    }

    public function delete($reference_id, $reference_uniqid)
    {
      $task = TasksModel::where('reference_id', $reference_id)
              ->where('reference_uniqid', $reference_uniqid)
              ->first();
      //echo $task->id;
      return $task;
    }

    public function getIdByReference($reference_id, $reference_uniqid)
    {
      $task = TasksModel::where('reference_id', $reference_id)
              ->where('reference_uniqid', $reference_uniqid)
              ->first();
      //echo $task->id;
      return $task ? $task->id : false;
    }
}
