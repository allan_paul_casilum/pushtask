<?php

namespace App\Tasks;

use Illuminate\Database\Eloquent\Model;

class TasksModel extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'tasks';
  protected $primaryKey = 'id';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['reference_id', 'reference_uniqid', 'user_id', 'title', 'description', 'assign_name', 'assign_email', 'status'];

  public function user()
  {
      return $this->belongsTo('App\User');
  }

  public function reminders()
  {
      return $this->hasMany('App\Reminders\ReminderModel', 'task_id');
  }
}
