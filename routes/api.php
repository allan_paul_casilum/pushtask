<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::post('login', 'PassportController@login');
Route::post('register', 'PassportController@register');
//no need this route get-all-tasks
Route::get('get-all-tasks', 'Tasks\API\TasksController@getAllTasks');
Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@details');
    Route::put('user/{user_id}', 'Users\API\APIUsers@update');
    Route::resource('tasks', 'Tasks\API\TasksController');
    Route::post('tasks/store-job', 'Tasks\API\TasksController@storeToJob');
    Route::post('tasks/update-job', 'Tasks\API\TasksController@updateTasks');
    Route::post('tasks/remove', 'Tasks\API\TasksController@removeTasks');
    Route::get('tasks-owner', 'Tasks\API\TasksController@getOwnerTasks');
    Route::get('send-tasks-email', 'Tasks\API\TasksController@sendTasksEmail');
    Route::resource('reminders', 'Reminder\API\ReminderController');
    //Route::post('tasks', 'Tasks\API\TasksController@store');
});
