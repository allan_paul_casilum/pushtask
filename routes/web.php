<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    //return TasksOrm::test();
});
Route::get('/test-mail', function(){
  Mail::send('email-test',[], function($message) {
    $message->to('youremail@domain.com')->subject('Testing email');
  });
});
Route::get('email-jobs', 'TestJobController@processQueue');
