<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountApiToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('api_token', 60)->unique()->nullable();
            $table->char('role', 20)->after('email_verified_at');
            $table->integer('parent')->after('role')->default(0);
            $table->integer('active')->after('parent')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['api_token']);
            $table->dropColumn(['role']);
            $table->dropColumn(['parent']);
            $table->dropColumn(['active']);
        });
    }
}
