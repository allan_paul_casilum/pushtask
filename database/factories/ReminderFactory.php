<?php

use App\Tasks\TasksModel;
use App\Reminders\ReminderModel;
use Faker\Generator as Faker;

$factory->define(ReminderModel::class, function (Faker $faker) {
    return [
        'task_id' => TasksModel::all()->random()->id,
        'title' => $faker->sentence,
        'send_every' => $faker->randomElement(['hourly', 'daily', 'everyMinute']),
        'priority' => $faker->numberBetween(0,10),
    ];
});
