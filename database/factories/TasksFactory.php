<?php

use App\Tasks\TasksModel;
use App\User;
use Faker\Generator as Faker;

$factory->define(TasksModel::class, function (Faker $faker) {
    return [
        'reference_id' => 0,
        'reference_uniqid' => 0,
        'user_id' => User::all()->random()->id,
        'title' => $faker->sentence,
        'assign_name' => $faker->name,
        'assign_email' => $faker->unique()->safeEmail,
        'title' => $faker->sentence,
        'description' => $faker->sentence,
        'status' => $faker->randomElement(['start', 'done', 'stop', 'idle'])
    ];
});
